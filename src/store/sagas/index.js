// import ApiErrors from "./ApiErrors";

// export default [...ApiErrors];

import { fork } from 'redux-saga/effects';
import { saga as eog } from './eog';

function* rootSaga () {
	yield fork( eog );
}

export default rootSaga;